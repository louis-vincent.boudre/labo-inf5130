# Le code original se trouve ici : 
# https://www.geeksforgeeks.org/python-program-for-n-queen-problem-backtracking-3/
# J'ai modifié l'algorithme pour remplir la matrice de résultat colonne par 
# colonne au lieu de ranger par ranger.
# ATTENTION ce code s'exécute avec python2


global N 
N = 8

def printSolution(board): 
	for i in range(N): 
		for j in range(N): 
			print board[i][j], 
		print


def is_safe(board, row, col): 

	# Check this row on left side 
	for i in range(col): 
		if board[row][i] == 1: 
			return False

        # Haut NOTE: j'ai ajouté ce check ici aussi
        for i in range(row):
                if board[i][col] == 1:
                    return False

	# Check upper diagonal on left side 
	for i, j in zip(range(row, -1, -1), range(col, -1, -1)): 
		if board[i][j] == 1: 
			return False

	# Check lower diagonal on left side 
	for i, j in zip(range(row, N, 1), range(col, -1, -1)): 
		if board[i][j] == 1: 
			return False

	return True

def solve_recurs(board, row): 
	if row == N: 
		return True

	for i in range(N): 
		if is_safe(board, row, i): 
			board[row][i] = 1

			if solve_recurs(board, row + 1) == True: 
				return True

			board[row][i] = 0
	return False

def solve(): 

	board = [[0 for _ in range(N)] for _ in range(N)]

	if solve_recurs(board, 0) == False: 
		print "Solution does not exist"
		return False

	printSolution(board) 
	return True

# driver program to test above function 
solve() 

# This code is contributed by Divyanshu Mehta 

