
fun produit_entier(xs: Array[Int], cible: Int): Bool
do
        var n = xs.length
        default_comparator.merge_sort(xs, 0, n-1)
        var i = 0
        var j = n-1
        var found = false
        while i < j and not found do
                var x = xs[i]
                var y = xs[j]
                var temp = x*y
                if temp == cible then
                        found = true
                else if temp > cible then
                        j -= 1
                else
                        i += 1
                end
        end
        return found
end


var xs = [2,10,14,95,100,2]
assert produit_entier(xs, 4)
assert produit_entier(xs, 9500)
assert produit_entier(xs, 200)
assert not produit_entier(xs, 24)
assert not produit_entier(xs, 0)

