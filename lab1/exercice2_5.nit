
fun element_majoritaire(xs: Array[Int]): Bool
do
        default_comparator.merge_sort(xs, 0, xs.length - 1) # O(nlogn)

        var cnt = 1
        var previous = xs[0]
        var target = ((xs.length.to_f / 2.0).ceil).to_i
        var found = cnt >= target
        var i = 1
        while i < xs.length and not found do
                if xs[i] == previous then
                        cnt += 1
                        found = cnt >= target
                else
                        previous = xs[i]
                        cnt = 1
                end
                i += 1
        end
        if found then
                print "element majoritaire: {previous}"
        else
                print "aucun element majoritaire"
        end
        return found
end

var a = [1,1,1,2,2]
var b = [1,2]
var c = [1,2,1,2,1]
var e = [10]
element_majoritaire(a)
element_majoritaire(b)
element_majoritaire(c)
element_majoritaire(e)
