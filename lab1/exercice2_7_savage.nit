
# Attention, ceci est un code sauvage, ne jamais redéfinir
# une classe aussi général que Array pour faire des trucs
# spécialisé. L'héritage serait plus approprié.
redef class Array[E: Int]

        # Implémentation de marde
        fun slice(from: Int, to: Int): Array[E]
        do
                var res = new Array[E]
                if to == -1 then
                        to = self.length
                end
                for i in [from..to[ do res.push(self[i])
                return res
        end


        # Ici, le `+` est une opération binaire dont
        # la vrai signature devrait être : (MaxMin, MaxMin) -> MaxMin.
        # Elle combine deux MaxMin
        # sauvage
        redef fun +(other)
        do
                var max = self[0]
                var min = self[1]
                return [max.max(other[0]), min.min(other[1])]
        end

        # sauvage
        fun max_min
        do
                if self[0] < self[1] then swap_at(0,1)
        end
end

# Code cool
fun bidon2(xs: Array[Int]): Array[Int]
do
        xs.max_min
        if xs.length == 2 then return xs
        var rest = xs.slice(2,-1)
        var ys = xs.slice(0,2)
        return ys + bidon2(rest)
end

var xs = [-7,2,-3,5,0,1]
print(bidon2(xs))
