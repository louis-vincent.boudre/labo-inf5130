
redef class Array[E: Int]

        # Implémentation de marde
        fun slice(from: Int, to: Int): Array[E]
        do
                var res = new Array[E]
                if to == -1 then
                        to = self.length
                end
                for i in [from..to[ do res.push(self[i])
                return res
        end
end

class MaxMin
        super Array[Int]
        protected var max: Int
        protected var min: Int

        redef init
        do
                push(max)
                push(min)
        end

        redef fun [](i)
        do
                assert i < 2 and i >= 0
                return super
        end

        redef fun []=(i, item)
        do
                assert i < 2 and i >= 0
                super
        end
end

fun bidon(xs: Array[Int], bi: Int, bs: Int): MaxMin
do
        print "ENTERING bi: {bi}, bs: {bs}, xs: {xs.slice(bi, -1)}"
        var max = xs[bi]
        var min = xs[bi+1]
        if xs[bi+1] > max then
                min = max
                max = xs[bi+1]
        end

        if bs <= bi + 2 then
                print "LEAVING bi: {bi}, bs: {bs}, max: {max}, min: {min}, xs: {xs.slice(bi, -1)}"
                return new MaxMin(max, min)
        end

        var temp = bidon(xs, bi+2, bs)
        if max < temp[0] then
                max = temp[0]
        end
        if min > temp[1] then
                min = temp[1]
        end
        print "LEAVING bi: {bi}, bs: {bs}, max: {max}, min: {min}, xs: {xs.slice(bi, -1)}"
        return new MaxMin(max, min)
end

var xs = [-7,2,-3,5,0,1]
print(bidon(xs, 0, xs.length-1))

#ENTERING bi: 0, bs: 5, xs: [-7,2,-3,5,0,1]
#ENTERING bi: 2, bs: 5, xs: [-3,5,0,1]
#ENTERING bi: 4, bs: 5, xs: [0,1]
#LEAVING bi: 4, bs: 5, max: 1, min: 0, xs: [0,1]
#LEAVING bi: 2, bs: 5, max: 5, min: -3, xs: [-3,5,0,1]
#LEAVING bi: 0, bs: 5, max: 5, min: -7, xs: [-7,2,-3,5,0,1]
#[5,-7]
