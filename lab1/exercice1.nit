

class Bits
        super Array[Int]

        init from_arr(arr: Array[Int])
        do
                add_all(arr)
        end

        init from_str(str: String)
        do
                for c in str.reversed do
                        if c == '1' then
                                push(1)
                        else
                                push(0)
                        end
                end
        end

        # exercice 2.1
        redef fun +(b): Bits
        do
                var a = self
                var n = a.length.max(b.length)
                var padding = (a.length - b.length).abs
                if b.length < a.length then
                        b.as(Bits).extends(padding)
                else if a.length < b.length then
                        a.extends(padding)
                end
                var res = new Array[Int].with_capacity(n + 1)
                var carry = 0
                for i in [0..n[ do
                        var x = a[i]
                        var y = b[i]
                        var sum = (x + y + carry) % 2
                        carry = (x & carry) | (y & carry) | (x & y)
                        res[i] = sum
                end
                res[n] = carry
                return new Bits.from_arr(res)
        end

        fun extends(padding: Int)
        do
                for i in [0..padding[ do push(0)
        end

        fun copier(decalage: Int): Bits
        do
                var n = self.length
                var res = new Bits.with_capacity(n + decalage)
                for i in [0..decalage[ do res[i] = 0
                for i in [0..n[ do res[i+decalage] = self[i]
                for i in [decalage + n..2*n[ do res[i] = 0
                return res
        end

        # exercice 2.2
        fun mult(b: Bits): Bits
        do
                var n = self.length
                var acc = new Bits
                for i in [0..n[ do
                        if b[i] == 0 then continue
                        var temp = copier(i)
                        acc += temp
                end
                return acc
        end

        redef fun to_s
        do
                var res = ""
                for i in self.reversed do res += i.to_s
                return res
        end
end

var b1 = new Bits.from_str("1100")
var b2 = new Bits.from_str("0011")
var b3 = new Bits.from_str("0001")
var b4 = new Bits.from_str("1111")
var b5 = new Bits.from_str("0010")
var zero = new Bits
print b1 + zero
print b1 + b2
print b3 + b1
print b2 + b3
print b4 + b4
print b1.mult(b1)
