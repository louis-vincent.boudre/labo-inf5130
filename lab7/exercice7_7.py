import heapq

def kfusion(tabs):
    k = len(tabs)
    indices = [0] * k
    res = []
    fin_fusion = False
    while not fin_fusion:
        min_tab = -1
        j = -1
        for i, indice in enumerate(indices):
            if indice == len(tabs[i]): continue
            if min_tab == -1 or tabs[i][indice] < tabs[min_tab][j]:
                min_tab = i
                j = indice
        if j == -1:
            fin_fusion = True
        else:
            res.append(tabs[min_tab][j])
            indices[min_tab] += 1

    print(res)

def kfusion2(tabs):
    k = len(tabs)
    indices = [0] * k
    q = []
    for i in range(k):
        heapq.heappush(q, (tabs[i][0], i))
    res = []
    fin_fusion = False
    while len(q) > 0:
        x, i = heapq.heappop(q)
        res.append(x)
        indices[i] += 1
        j = indices[i]
        if j < len(tabs[i]):
            heapq.heappush(q, (tabs[i][j], i))
    print(res)

    

kfusion([[4,8,10], [1,3,5], [2,7,9]])
kfusion2([[4,8,10], [1,3,5], [2,7,9]])
