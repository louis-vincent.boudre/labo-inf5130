
def f(tab):
    # O(n^2)
    n = len(tab)
    M = [[tab[i]] for i in range(n)]
    for i in range(n):  # Theta(n)
        for j in range(i):    # O(n)
            if tab[i] > tab[j] and len(M[i]) < len(M[j]) + 1:
                M[i] = M[j] + [tab[i]]
    
    # Sélectionne la valeur mémorisé ayant la plus grande taille.
    res = max(M, key=len)
    return res


f2([5,3,2,1])

assert f([1,2,3,4,5]) == [1,2,3,4,5]
assert f([2,1,2,3,4]) == [1,2,3,4]
assert f([5,4,3,2,1]) == [5]
assert f([5,4,2,3,1,5]) == [2,3,5]
assert f([5,4,3,2,1,2]) == [1,2]
