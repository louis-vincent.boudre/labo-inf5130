lorem = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."


# Nombre maximal de caractère sur une ligne
PAGE_WIDTH = 6
INF = 0xFFFFFFFF
# Exercice 6.9 (Text justification)


# Source:  https://www.youtube.com/watch?v=RORuwHiblPc
# https://www.youtube.com/watch?v=ENyox7kNKeY&list=PLcDimPvbmfT8qAxD6JH_kmXiQwTNcoK78&index=2
# Allez voir ce vidéo, sinon le fucking corriger est incompréhensible.

def badness(ws,i,j):
    total = sum([len(w) for w in ws[i:j+1]]) + j - i
    if total > PAGE_WIDTH: return INF
    # On utilise 2 au lieu de 3 pour avoir des plus petites
    # valeurs.
    return (PAGE_WIDTH-total)**2

def justify(words):
    n = len(words)
    
    # Matrice de coûts
    C = [[0] * n for _ in range(n)]

    for i in range(n):
        for j in range(i, n):
            C[i][j] = badness(words, i, j)
    
    # Ce code a provient de : https://github.com/mission-peace/interview/blob/master/src/com/interview/dynamic/TextJustification.java
    # Je l'ai seulement traduit en Python.
    # De plus, 
    minC = [0]*n
    result = [0]*n
    for i in range(n-1,-1,-1):
        minC[i] = C[i][n-1]
        result[i] = n
        for j in range(n-1, i, -1):
            if C[i][j-1] == INF:
                continue
            if minC[i] > minC[j] + C[i][j-1]:
                minC[i] = minC[j] + C[i][j-1]
                result[i] = j

    print("Min C: " + str(minC[0]), minC)
    print(result)

toto = ["aaa", "bb", "cc", "ddddd"]
justify(toto)
