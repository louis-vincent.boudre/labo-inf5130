
M = [[0,1,2,3,4,5,6],
     [1,1,1,2,3,4,5],
     [2,1,2,2,2,3,4],
     [3,2,2,2,3,2,3],
     [4,3,3,2,3,3,3],
     [5,4,4,3,3,3,4],
     [6,5,5,4,4,4,3]]


def f(M, s1, s2):
    n = len(s1)
    m = len(s2)
    INS = 1
    REP = 2
    DEL = 3
    def inner(i, j, moves):
        if i == 0 and j == 0:
            return moves

        ins = M[i][j-1]
        delete = M[i-1][j]
        repl = M[i-1][j-1]
        
        if M[i][j] == ins + 1:
            return inner(i, j-1, [1] + moves)   
        elif M[i][j] == repl or M[i][j] == repl + 1:
            return inner(i-1, j-1, [2] + moves)
        else:
            return inner(i-1, j, [3] + moves)
    
    ms = inner(n, m, [])

    i = 0
    j = 0
    print(s1 + " TO " + s2)
    for m in ms:
        if m == INS:
            s1 = s1[:i] + s2[j] + s1[i:]
            j += 1 
            i += 1 
            print("INS: " + s1)
        elif m == REP:
            s1 = s1[:i] + s2[j] + s1[i+1:]
            i += 1
            j += 1
            print("REP: "+ s1)
        else:
            s1 = s1[0:i] + s1[i+1:]
            i + 1
            print("DEL: " + s1)


f(M, "ACGGGT", "CAGCGT")
